




// DO NOT EDIT !
// This file is generated using the MantaFlow preprocessor (prep generate).




#line 1 "C:/Users/China/Desktop/manta_0_12/source/conjugategrad.h"
/******************************************************************************
 *
 * MantaFlow fluid solver framework
 * Copyright 2011 Tobias Pfaff, Nils Thuerey 
 *
 * This program is free software, distributed under the terms of the
 * Apache License, Version 2.0 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Conjugate gradient solver
 *
 ******************************************************************************/

#ifndef _CONJUGATEGRADIENT_H
#define _CONJUGATEGRADIENT_H

#include "vectorbase.h"
#include "grid.h"
#include "kernel.h"
#include "multigrid.h"

namespace Manta { 

static const bool CG_DEBUG = false;

//! Basic CG interface 
class GridCgInterface {
	public:
		enum PreconditionType { PC_None=0, PC_ICP, PC_mICP, PC_MGP };
		
		GridCgInterface() : mUseL2Norm(true) {};
		virtual ~GridCgInterface() {};

		// solving functions
		virtual bool iterate() = 0;
		virtual void solve(int maxIter) = 0;

		// precond
		virtual void setICPreconditioner(PreconditionType method, Grid<Real> *A0, Grid<Real> *Ai, Grid<Real> *Aj, Grid<Real> *Ak) = 0;
		virtual void setMGPreconditioner(PreconditionType method, GridMg* MG) = 0;

		// access
		virtual Real getSigma() const = 0;
		virtual Real getIterations() const = 0;
		virtual Real getResNorm() const = 0;
		virtual void setAccuracy(Real set) = 0;
		virtual Real getAccuracy() const = 0;

		//! force reinit upon next iterate() call, can be used for doing multiple solves
		virtual void forceReinit() = 0;

		void setUseL2Norm(bool set) { mUseL2Norm = set; }

	protected:

		// use l2 norm of residualfor threshold? (otherwise uses max norm)
		bool mUseL2Norm; 
};


//! Run single iteration of the cg solver
/*! the template argument determines the type of matrix multiplication,
	typically a ApplyMatrix kernel, another one is needed e.g. for the
	mesh-based wave equation solver */
template<class APPLYMAT>
class GridCg : public GridCgInterface {
	public:
		//! constructor
		GridCg(Grid<Real>& dst, Grid<Real>& rhs, Grid<Real>& residual, Grid<Real>& search, const FlagGrid& flags, Grid<Real>& tmp, 
				Grid<Real>* A0, Grid<Real>* pAi, Grid<Real>* pAj, Grid<Real>* pAk);
		~GridCg() {}
		
		void doInit();
		bool iterate();
		void solve(int maxIter);
		//! init pointers, and copy values from "normal" matrix
		void setICPreconditioner(PreconditionType method, Grid<Real> *A0, Grid<Real> *Ai, Grid<Real> *Aj, Grid<Real> *Ak);
		void setMGPreconditioner(PreconditionType method, GridMg* MG);
		void forceReinit() { mInited = false; }
		
		// Accessors        
		Real getSigma() const { return mSigma; }
		Real getIterations() const { return mIterations; }

		Real getResNorm() const { return mResNorm; }

		void setAccuracy(Real set) { mAccuracy=set; }
		Real getAccuracy() const { return mAccuracy; }

	protected:
		bool mInited;
		int mIterations;
		// grids
		Grid<Real>& mDst;
		Grid<Real>& mRhs;
		Grid<Real>& mResidual;
		Grid<Real>& mSearch;
		const FlagGrid& mFlags;
		Grid<Real>& mTmp;

		Grid<Real> *mpA0, *mpAi, *mpAj, *mpAk;

		PreconditionType mPcMethod;
		//! preconditioning grids
		Grid<Real> *mpPCA0, *mpPCAi, *mpPCAj, *mpPCAk;
		GridMg* mMG;

		//! sigma / residual
		Real mSigma;
		//! accuracy of solver (max. residuum)
		Real mAccuracy;
		//! norm of the residual
		Real mResNorm;
}; // GridCg


//! Kernel: Apply symmetric stored Matrix



 struct ApplyMatrix : public KernelBase { ApplyMatrix(const FlagGrid& flags, Grid<Real>& dst, const Grid<Real>& src, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak) :  KernelBase(&flags,0) ,flags(flags),dst(dst),src(src),A0(A0),Ai(Ai),Aj(Aj),Ak(Ak)   { runMessage(); run(); }   inline void op(IndexInt idx, const FlagGrid& flags, Grid<Real>& dst, const Grid<Real>& src, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak )  {
	if (!flags.isFluid(idx)) {
		dst[idx] = src[idx]; return;
	}    

	dst[idx] =  src[idx] * A0[idx]
				+ src[idx-X] * Ai[idx-X]
				+ src[idx+X] * Ai[idx]
				+ src[idx-Y] * Aj[idx-Y]
				+ src[idx+Y] * Aj[idx]
				+ src[idx-Z] * Ak[idx-Z] 
				+ src[idx+Z] * Ak[idx];
}    inline const FlagGrid& getArg0() { return flags; } typedef FlagGrid type0;inline Grid<Real>& getArg1() { return dst; } typedef Grid<Real> type1;inline const Grid<Real>& getArg2() { return src; } typedef Grid<Real> type2;inline Grid<Real>& getArg3() { return A0; } typedef Grid<Real> type3;inline Grid<Real>& getArg4() { return Ai; } typedef Grid<Real> type4;inline Grid<Real>& getArg5() { return Aj; } typedef Grid<Real> type5;inline Grid<Real>& getArg6() { return Ak; } typedef Grid<Real> type6; void runMessage() { debMsg("Executing kernel ApplyMatrix ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {   const IndexInt _sz = size; 
#pragma omp parallel 
 {  
#pragma omp for  
  for (IndexInt i = 0; i < _sz; i++) op(i,flags,dst,src,A0,Ai,Aj,Ak);  }   } const FlagGrid& flags; Grid<Real>& dst; const Grid<Real>& src; Grid<Real>& A0; Grid<Real>& Ai; Grid<Real>& Aj; Grid<Real>& Ak;   };
#line 121 "conjugategrad.h"



//! Kernel: Apply symmetric stored Matrix. 2D version



 struct ApplyMatrix2D : public KernelBase { ApplyMatrix2D(const FlagGrid& flags, Grid<Real>& dst, const Grid<Real>& src, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak) :  KernelBase(&flags,0) ,flags(flags),dst(dst),src(src),A0(A0),Ai(Ai),Aj(Aj),Ak(Ak)   { runMessage(); run(); }   inline void op(IndexInt idx, const FlagGrid& flags, Grid<Real>& dst, const Grid<Real>& src, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak )  {
	unusedParameter(Ak); // only there for parameter compatibility with ApplyMatrix
	
	if (!flags.isFluid(idx)) {
		dst[idx] = src[idx]; return;
	}    

	dst[idx] =  src[idx] * A0[idx]
				+ src[idx-X] * Ai[idx-X]     
				+ src[idx+X] * Ai[idx]
				+ src[idx-Y] * Aj[idx-Y]
				+ src[idx+Y] * Aj[idx];
	
	/*/对应于拉普拉斯矩阵*/
	//dst(i,j)=(1+(4*miu))*src(i,j)-src(i-1,j)-src(i+1,j)-src(i,j-1)-src(i,j+1);
}    inline const FlagGrid& getArg0() { return flags; } typedef FlagGrid type0;inline Grid<Real>& getArg1() { return dst; } typedef Grid<Real> type1;inline const Grid<Real>& getArg2() { return src; } typedef Grid<Real> type2;inline Grid<Real>& getArg3() { return A0; } typedef Grid<Real> type3;inline Grid<Real>& getArg4() { return Ai; } typedef Grid<Real> type4;inline Grid<Real>& getArg5() { return Aj; } typedef Grid<Real> type5;inline Grid<Real>& getArg6() { return Ak; } typedef Grid<Real> type6; void runMessage() { debMsg("Executing kernel ApplyMatrix2D ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {   const IndexInt _sz = size; 
#pragma omp parallel 
 {  
#pragma omp for  
  for (IndexInt i = 0; i < _sz; i++) op(i,flags,dst,src,A0,Ai,Aj,Ak);  }   } const FlagGrid& flags; Grid<Real>& dst; const Grid<Real>& src; Grid<Real>& A0; Grid<Real>& Ai; Grid<Real>& Aj; Grid<Real>& Ak;   };
#line 139 "conjugategrad.h"






 struct ApplyMatrixVarational : public KernelBase { ApplyMatrixVarational(const FlagGrid& flags, Grid<Vec3>& dst, const Grid<Vec3>& src, const Grid<Real>& Vp, const MACGrid& Vedge,const MACGrid& Vface ) :  KernelBase(&flags,1) ,flags(flags),dst(dst),src(src),Vp(Vp),Vedge(Vedge),Vface(Vface)   { runMessage(); run(); }  inline void op(int i, int j, int k, const FlagGrid& flags, Grid<Vec3>& dst, const Grid<Vec3>& src, const Grid<Real>& Vp, const MACGrid& Vedge,const MACGrid& Vface  )  {
	IndexInt idx = flags.index(i, j, k);
	if (!flags.isFluid(idx))
	{
		dst[idx] = src[idx];
		return;
	}	
	dst[idx].x = src[idx].x*(Vp(i, j, k)*2. + Vp(i - 1, j, k)*2. + Vedge(i, j, k).z + Vedge(i, j + 1, k).z+Vedge(i,j,k+1).y+Vedge(i,j,k).y)     
/*uxx*/	+ src[idx - X].x*(-Vp(i - 1, j, k)*2.)                                                                     //(i-1,j,k).x的系数
		+ src[idx + X].x*(-Vp(i, j, k)*2.)                                                                       //(i+1,j,k).x的系数
/*uyy*/ + src[idx - Y].x*(-Vedge(i, j, k).z)                                                                  //(i,j-1,k).x的系数
		+ src[idx + Y].x*(-Vedge(i, j + 1, k).z)                                                                //(i,j+1,k).x的系数
/*vxy*/ + src[idx + Y].y*(-Vedge(i, j + 1, k).z)                                                                //(i,j+1,k).y的系数
		+ src[idx + Y - X].y*(Vedge(i, j + 1, k).z)                                                           //(i-1,j+1,k).y的系数
		+ src[idx].y *(Vedge(i, j, k).z)                                                                   //(i,j,k).y的系数
		+ src[idx - X].y*(-Vedge(i, j, k).z)
/*uzz*/ + src[idx - Z].x*(-Vedge(i, j, k).y)
		+ src[idx + Z].x*(-Vedge(i, j, k + 1).y)
/*wxz*/ + src[idx + Z].z*(-Vedge(i, j, k + 1).y)
		+ src[idx + Z - X].z*(Vedge(i, j, k + 1).y)
		+ src[idx].z*(Vedge(i, j, k).y)
		+ src[idx - X].z*(-Vedge(i, j, k).y);

	dst[idx].y = src[idx].y*(Vp(i, j, k)*2. + Vp(i, j - 1, k)*2. + Vedge(i, j, k).x + Vedge(i, j, k + 1).x + Vedge(i, j, k).z + Vedge(i + 1, j, k).z)
/*vxx*/ + src[idx - X].y*(-Vedge(i, j, k).z)
		+ src[idx + X].y*(-Vedge(i + 1, j, k).z)
/*uyx*/ + src[idx + X].x*(-Vedge(i + 1, j, k).z)
		+ src[idx + X - Y].x*(Vedge(i + 1, j, k).z)
		+ src[idx].x*(Vedge(i, j, k).z)
		+ src[idx - Y].x*(-Vedge(i, j, k).z)
/*vyy*/ + src[idx - Y].y*(-Vp(i, j - 1, k)*2.)
		+ src[idx + Y].y*(-Vp(i, j, k)*2.)
/*vzz*/ + src[idx + Z].y*(-Vedge(i, j, k + 1).x)
		+ src[idx - Z].y*(-Vedge(i, j, k).x)
/*wyz*/ + src[idx + Z].z*(-Vedge(i, j, k + 1).x)
		+ src[idx + Z - Y].z*(Vedge(i, j, k + 1).x)
		+ src[idx].z*(Vedge(i, j, k).x)
		+ src[idx - Y].z*(-Vedge(i, j, k).x);
	
	dst[idx].z = src[idx].z*(Vp(i, j, k)*2. + Vp(i, j, k - 1)*2. + Vedge(i, j, k).x + Vedge(i, j + 1, k).x + Vedge(i, j, k).y + Vedge(i + 1, j, k).y)
/*wzz*/ + src[idx - Z].z*(-Vp(i, j, k - 1)*2.)
		+ src[idx + Z].z*(-Vp(i, j, k)*2.)
/*wxx*/ + src[idx - Z].z*(-Vedge(i, j, k).y)
		+ src[idx + Z].z*(-Vedge(i + 1, j, k).y)
/*uzx*/ + src[idx + X].x*(-Vedge(i + 1, j, k).y)
		+ src[idx + X - Z].x*(Vedge(i + 1, j, k).y)
		+ src[idx].x*(Vedge(i, j, k).y)
		+ src[idx - Z].x*(-Vedge(i, j, k).y)
/*wyy*/ + src[idx - Y].z*(-Vedge(i, j, k).x)
		+ src[idx + Y].z*(-Vedge(i, j + 1, k).x)
/*vzy*/ + src[idx + Y].y*(-Vedge(i, j + 1, k).x)
		+ src[idx + Y - Z].y*(Vedge(i, j + 1, k).x)
		+ src[idx].y*(Vedge(i, j, k).x)
		+ src[idx - Z].y*(-Vedge(i, j, k).x);

	dst[idx].x /= (Vface(i, j, k).x);
	dst[idx].y /= (Vface(i, j, k).y);
	dst[idx].z /= (Vface(i, j, k).z);

}   inline const FlagGrid& getArg0() { return flags; } typedef FlagGrid type0;inline Grid<Vec3>& getArg1() { return dst; } typedef Grid<Vec3> type1;inline const Grid<Vec3>& getArg2() { return src; } typedef Grid<Vec3> type2;inline const Grid<Real>& getArg3() { return Vp; } typedef Grid<Real> type3;inline const MACGrid& getArg4() { return Vedge; } typedef MACGrid type4;inline const MACGrid& getArg5() { return Vface; } typedef MACGrid type5; void runMessage() { debMsg("Executing kernel ApplyMatrixVarational ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {  const int _maxX = maxX; const int _maxY = maxY; if (maxZ > 1) { 
#pragma omp parallel 
 {  
#pragma omp for  
  for (int k=minZ; k < maxZ; k++) for (int j=1; j < _maxY; j++) for (int i=1; i < _maxX; i++) op(i,j,k,flags,dst,src,Vp,Vedge,Vface);  } } else { const int k=0; 
#pragma omp parallel 
 {  
#pragma omp for  
  for (int j=1; j < _maxY; j++) for (int i=1; i < _maxX; i++) op(i,j,k,flags,dst,src,Vp,Vedge,Vface);  } }  } const FlagGrid& flags; Grid<Vec3>& dst; const Grid<Vec3>& src; const Grid<Real>& Vp; const MACGrid& Vedge; const MACGrid& Vface;   };
#line 159 "conjugategrad.h"


//! Kernel: Construct the matrix for the poisson equation

 struct MakeLaplaceMatrix : public KernelBase { MakeLaplaceMatrix(const FlagGrid& flags, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak, const MACGrid* fractions = 0) :  KernelBase(&flags,1) ,flags(flags),A0(A0),Ai(Ai),Aj(Aj),Ak(Ak),fractions(fractions)   { runMessage(); run(); }  inline void op(int i, int j, int k, const FlagGrid& flags, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak, const MACGrid* fractions = 0 )  {
	if (!flags.isFluid(i,j,k))
		return;
	
	if(!fractions) {
		// diagonal, A0   
		if (!flags.isObstacle(i-1,j,k))                 A0(i,j,k) += 1.;
		if (!flags.isObstacle(i+1,j,k))                 A0(i,j,k) += 1.;
		if (!flags.isObstacle(i,j-1,k))                 A0(i,j,k) += 1.;
		if (!flags.isObstacle(i,j+1,k))                 A0(i,j,k) += 1.;
		if (flags.is3D() && !flags.isObstacle(i,j,k-1)) A0(i,j,k) += 1.;
		if (flags.is3D() && !flags.isObstacle(i,j,k+1)) A0(i,j,k) += 1.;
		
		// off-diagonal entries       为什么只计算+1的格子?
		if (flags.isFluid(i+1,j,k))                 Ai(i,j,k) = -1.;
		if (flags.isFluid(i,j+1,k))                 Aj(i,j,k) = -1.;
		if (flags.is3D() && flags.isFluid(i,j,k+1)) Ak(i,j,k) = -1.;
	} else {
		// diagonal
		A0(i,j,k)                   += fractions->get(i  ,j,k).x;
		A0(i,j,k)                   += fractions->get(i+1,j,k).x;
		A0(i,j,k)                   += fractions->get(i,j  ,k).y;
		A0(i,j,k)                   += fractions->get(i,j+1,k).y;
		if (flags.is3D()) A0(i,j,k) += fractions->get(i,j,k  ).z;
		if (flags.is3D()) A0(i,j,k) += fractions->get(i,j,k+1).z;
		
		// off-diagonal entries
		if (flags.isFluid(i+1,j,k))                 Ai(i,j,k) = -fractions->get(i+1,j,k).x;
		if (flags.isFluid(i,j+1,k))                 Aj(i,j,k) = -fractions->get(i,j+1,k).y;
		if (flags.is3D() && flags.isFluid(i,j,k+1)) Ak(i,j,k) = -fractions->get(i,j,k+1).z;
	}
	
}   inline const FlagGrid& getArg0() { return flags; } typedef FlagGrid type0;inline Grid<Real>& getArg1() { return A0; } typedef Grid<Real> type1;inline Grid<Real>& getArg2() { return Ai; } typedef Grid<Real> type2;inline Grid<Real>& getArg3() { return Aj; } typedef Grid<Real> type3;inline Grid<Real>& getArg4() { return Ak; } typedef Grid<Real> type4;inline const MACGrid* getArg5() { return fractions; } typedef MACGrid type5; void runMessage() { debMsg("Executing kernel MakeLaplaceMatrix ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {  const int _maxX = maxX; const int _maxY = maxY; if (maxZ > 1) { 
#pragma omp parallel 
 {  
#pragma omp for  
  for (int k=minZ; k < maxZ; k++) for (int j=1; j < _maxY; j++) for (int i=1; i < _maxX; i++) op(i,j,k,flags,A0,Ai,Aj,Ak,fractions);  } } else { const int k=0; 
#pragma omp parallel 
 {  
#pragma omp for  
  for (int j=1; j < _maxY; j++) for (int i=1; i < _maxX; i++) op(i,j,k,flags,A0,Ai,Aj,Ak,fractions);  } }  } const FlagGrid& flags; Grid<Real>& A0; Grid<Real>& Ai; Grid<Real>& Aj; Grid<Real>& Ak; const MACGrid* fractions;   };
#line 221 "conjugategrad.h"


//Ai,Aj,Ak分别表示沿着空间格子的三个方向的系数，   一个cell的x,y,z分量则表示一个式子使用到的速度分量
//由原来的求一个变为求三个
//

 struct MakeVariationalMatrix : public KernelBase { MakeVariationalMatrix(const FlagGrid &flags, Grid<Real>& A_vp, Grid<Real>& A_12, Grid<Real>& A_13,Grid<Real>& A_23,const Grid<Real>& Vp,const MACGrid& fractions_edge) :  KernelBase(&flags,1) ,flags(flags),A_vp(A_vp),A_12(A_12),A_13(A_13),A_23(A_23),Vp(Vp),fractions_edge(fractions_edge)   { runMessage(); run(); }  inline void op(int i, int j, int k, const FlagGrid &flags, Grid<Real>& A_vp, Grid<Real>& A_12, Grid<Real>& A_13,Grid<Real>& A_23,const Grid<Real>& Vp,const MACGrid& fractions_edge )  {
	if (!flags.isFluid(i, j, k))
		return;
	//u-velocity
	//考虑对角线，
	
// 		A0u(i, j, k).x += Vp(i, j, k)*2.;            //A0u(i,j,k).x 当作(i,j,k).x的系数
// 		A0u(i, j, k).x += Vp(i + 1, j, k)* 2.;
// 		A0u(i, j, k).x += fractions_edge(i, j , k).z;
// 		A0u(i, j, k).x += fractions_edge(i, j+1, k).z;
// 		
// 		Aiu(i, j, k).x = -Vp(i + 1, j, k);         //Aiu(i,j,k).x当作（i+1,j,k）.x的系数，（i-1,j,k）.x的系数由Aiu(i-1,j,k).x推出
// 	
// 		Aju(i, j, k).x = -fractions_edge(i, j+1, k).z;    //Aju(i, j, k).x当作(i,j+1,k).x的系数，(i,j-1,k).x的系数由Aju(i, j-1, k).x推出
// 	   
// 		//剩下四个系数其实和上面两个一样
// 		Aiu(i, j, k).y = -fractions_edge(i, j+1, k).z;          //Aiu(i, j, k).y当作(i+1,j,k).y的系数
// 		A0u(i, j, k).y = fractions_edge(i, j+1, k).z;          //A0u(i, j, k).y当作(i,j,k).y的系数
// 		Aju(i, j, k).y = fractions_edge(i, j , k).z;          //Aju(i, j, k).y当作(i,j+1,k)的系数   (i-1,j+1,k).y的系数取反即可
	
	//	Aiu(i, j, k).y += fractions_edge(i, j, k).z;
    //		Aiu(i, j, k).y += fractions_edge(i, j-1, k).z;
	
}   inline const FlagGrid& getArg0() { return flags; } typedef FlagGrid type0;inline Grid<Real>& getArg1() { return A_vp; } typedef Grid<Real> type1;inline Grid<Real>& getArg2() { return A_12; } typedef Grid<Real> type2;inline Grid<Real>& getArg3() { return A_13; } typedef Grid<Real> type3;inline Grid<Real>& getArg4() { return A_23; } typedef Grid<Real> type4;inline const Grid<Real>& getArg5() { return Vp; } typedef Grid<Real> type5;inline const MACGrid& getArg6() { return fractions_edge; } typedef MACGrid type6; void runMessage() { debMsg("Executing kernel MakeVariationalMatrix ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {  const int _maxX = maxX; const int _maxY = maxY; if (maxZ > 1) { 
#pragma omp parallel 
 {  
#pragma omp for  
  for (int k=minZ; k < maxZ; k++) for (int j=1; j < _maxY; j++) for (int i=1; i < _maxX; i++) op(i,j,k,flags,A_vp,A_12,A_13,A_23,Vp,fractions_edge);  } } else { const int k=0; 
#pragma omp parallel 
 {  
#pragma omp for  
  for (int j=1; j < _maxY; j++) for (int i=1; i < _maxX; i++) op(i,j,k,flags,A_vp,A_12,A_13,A_23,Vp,fractions_edge);  } }  } const FlagGrid& flags; Grid<Real>& A_vp; Grid<Real>& A_12; Grid<Real>& A_13; Grid<Real>& A_23; const Grid<Real>& Vp; const MACGrid& fractions_edge;   };
#line 258 "conjugategrad.h"


// KERNEL(bnd = 1)
// void MakeVariationalMatrix(const FlagGrid& flags, Grid<Vec3>& A0_vp, Grid<Real>& A12, Grid<Real>& A13, Grid<Real>& A23, const Grid<Real>& Vp, const MACGrid& fractions_edge=0) {
// 	if (!flags.isFluid(i, j, k))
// 		return;
// 	//(i, j, k).x的系数
// 	A0_vp(i,j,k).x+= Vp(i, j, k)*2.;
// 	A0_vp(i,j,k).x+= Vp(i + 1, j, k)* 2.;
// 	A0_vp(i, j, k).x += fractions_edge(i, j, k).z;
// 	A0_vp(i, j, k).x += fractions_edge(i, j + 1, k).z;
// 	//(i,j+1,k).x的系数
// 	A12(i,j,k)=--fractions_edge(i, j + 1, k).z
// 
// }

} // namespace

#endif 



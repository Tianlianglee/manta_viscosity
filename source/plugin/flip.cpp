/******************************************************************************
 *
 * MantaFlow fluid solver framework 
 * Copyright 2011 Tobias Pfaff, Nils Thuerey 
 *
 * This program is free software, distributed under the terms of the
 * Apache License, Version 2.0 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * FLIP (fluid implicit particles)
 * for use with particle data fields
 *
 ******************************************************************************/

#include "particle.h"
#include "grid.h"
#include "commonkernels.h"
#include "randomstream.h"
#include "levelset.h"
#include "shapes.h"
#include "matrixbase.h"

using namespace std;
namespace Manta {



// init

//! note - this is a simplified version , sampleLevelsetWithParticles has more functionality
PYTHON() void sampleFlagsWithParticles(const FlagGrid& flags, BasicParticleSystem& parts,
				       const int discretization, const Real randomness)
{
	const bool is3D = flags.is3D();
	const Real jlen = randomness / discretization;
	const Vec3 disp (1.0 / discretization, 1.0 / discretization, 1.0/discretization);
	RandomStream mRand(9832);
 
	FOR_IJK_BND(flags, 0) {
		if ( flags.isObstacle(i,j,k) ) continue;
		if ( flags.isFluid(i,j,k) ) {
			const Vec3 pos (i,j,k);
			for (int dk=0; dk<(is3D ? discretization : 1); dk++)
			for (int dj=0; dj<discretization; dj++)
			for (int di=0; di<discretization; di++) {
				Vec3 subpos = pos + disp * Vec3(0.5+di, 0.5+dj, 0.5+dk);
				subpos += jlen * (Vec3(1,1,1) - 2.0 * mRand.getVec3());
				if(!is3D) subpos[2] = 0.5; 
				parts.addBuffered(subpos);
			}
		}
	}
	parts.insertBufferedParticles();
}

//! sample a level set with particles, use reset to clear the particle buffer,
//! and skipEmpty for a continuous inflow (in the latter case, only empty cells will
//! be re-filled once they empty when calling sampleLevelsetWithParticles during 
//! the main loop).
PYTHON() void sampleLevelsetWithParticles(const LevelsetGrid& phi, const FlagGrid& flags, BasicParticleSystem& parts,
					  const int discretization, const Real randomness, const bool reset=false, const bool refillEmpty=false)
{
	const bool is3D = phi.is3D();
	const Real jlen = randomness / discretization;     //0.025
	const Vec3 disp (1.0 / discretization, 1.0 / discretization, 1.0/discretization);    //0.5
	RandomStream mRand(9832);
 
	if(reset) {
		parts.clear(); 
		parts.doCompress();
	}

	FOR_IJK_BND(phi, 0) {
		if ( flags.isObstacle(i,j,k) ) continue;
		if ( refillEmpty && flags.isFluid(i,j,k) ) continue;
		if ( phi(i,j,k) < 1.733 ) {
			const Vec3 pos (i,j,k);
			for (int dk=0; dk<(is3D ? discretization : 1); dk++)
			for (int dj=0; dj<discretization; dj++)
			for (int di=0; di<discretization; di++) {
				Vec3 subpos = pos + disp * Vec3(0.5+di, 0.5+dj, 0.5+dk);
				subpos += jlen * (Vec3(1,1,1) - 2.0 * mRand.getVec3());
				if(!is3D) subpos[2] = 0.5; 
				if( phi.getInterpolated(subpos) > 0. ) continue; 
				parts.addBuffered(subpos);
			}
		}
	}

	parts.insertBufferedParticles();
}

//! sample a shape with particles, use reset to clear the particle buffer,
//! and skipEmpty for a continuous inflow (in the latter case, only empty cells will
//! be re-filled once they empty when calling sampleShapeWithParticles during
//! the main loop).
PYTHON() void sampleShapeWithParticles(const Shape& shape, const FlagGrid& flags, BasicParticleSystem& parts,
				       const int discretization, const Real randomness, const bool reset=false, const bool refillEmpty=false,
				       const LevelsetGrid *exclude=NULL)
{
	const bool is3D = flags.is3D();
	const Real jlen = randomness / discretization;
	const Vec3 disp (1.0 / discretization, 1.0 / discretization, 1.0/discretization);
	RandomStream mRand(9832);

	if(reset) {
		parts.clear();
		parts.doCompress();
	}

	FOR_IJK_BND(flags, 0) {
		if ( flags.isObstacle(i,j,k) ) continue;
		if ( refillEmpty && flags.isFluid(i,j,k) ) continue;
		const Vec3 pos (i,j,k);
		for (int dk=0; dk<(is3D ? discretization : 1); dk++)
		for (int dj=0; dj<discretization; dj++)
		for (int di=0; di<discretization; di++) {
			Vec3 subpos = pos + disp * Vec3(0.5+di, 0.5+dj, 0.5+dk);
			subpos += jlen * (Vec3(1,1,1) - 2.0 * mRand.getVec3());
			if(!is3D) subpos[2] = 0.5;
			if(exclude && exclude->getInterpolated(subpos) <= 0.) continue;
			if(!shape.isInside(subpos)) continue;
			parts.addBuffered(subpos);
		}
	}

	parts.insertBufferedParticles();
}

//! mark fluid cells and helpers
KERNEL() void knClearFluidFlags(FlagGrid& flags, int dummy=0) {
	if (flags.isFluid(i,j,k)) {
		flags(i,j,k) = (flags(i,j,k) | FlagGrid::TypeEmpty) & ~FlagGrid::TypeFluid;
	}
}
KERNEL(bnd=1) 
void knSetNbObstacle(FlagGrid& nflags, const FlagGrid& flags, const Grid<Real>& phiObs) {
	if ( phiObs(i,j,k)>0. ) return;
	if (flags.isEmpty(i,j,k)) {
		bool set=false;
		if( (flags.isFluid(i-1,j,k)) && (phiObs(i+1,j,k)<=0.) ) set=true;
		if( (flags.isFluid(i+1,j,k)) && (phiObs(i-1,j,k)<=0.) ) set=true;
		if( (flags.isFluid(i,j-1,k)) && (phiObs(i,j+1,k)<=0.) ) set=true;
		if( (flags.isFluid(i,j+1,k)) && (phiObs(i,j-1,k)<=0.) ) set=true;
		if(flags.is3D()) {
		if( (flags.isFluid(i,j,k-1)) && (phiObs(i,j,k+1)<=0.) ) set=true;
		if( (flags.isFluid(i,j,k+1)) && (phiObs(i,j,k-1)<=0.) ) set=true;
		}
		if(set) nflags(i,j,k) = (flags(i,j,k) | FlagGrid::TypeFluid) & ~FlagGrid::TypeEmpty;
	}
}
PYTHON() void markFluidCells(const BasicParticleSystem& parts, FlagGrid& flags, const Grid<Real>* phiObs=NULL, const ParticleDataImpl<int>* ptype=NULL, const int exclude=0) {
	// remove all fluid cells
	knClearFluidFlags(flags, 0);
	
	// mark all particles in flag grid as fluid
	for(IndexInt idx=0; idx<parts.size(); idx++) {
		if (!parts.isActive(idx) || (ptype && ((*ptype)[idx] & exclude))) continue;
		Vec3i p = toVec3i( parts.getPos(idx) );
		if (flags.isInBounds(p) && flags.isEmpty(p))
			flags(p) = (flags(p) | FlagGrid::TypeFluid) & ~FlagGrid::TypeEmpty;
	}

	// special for second order obstacle BCs, check empty cells in boundary region
	if(phiObs) {
		FlagGrid tmp(flags);
		knSetNbObstacle(tmp, flags, *phiObs);
		flags.swap(tmp);
	}
}

// for testing purposes only...
PYTHON() void testInitGridWithPos(Grid<Real>& grid) {
	FOR_IJK(grid) { grid(i,j,k) = norm( Vec3(i,j,k) ); }
}



//! helper to calculate particle radius factor to cover the diagonal of a cell in 2d/3d
inline Real calculateRadiusFactor(const Grid<Real>& grid, Real factor) {
	return (grid.is3D() ? sqrt(3.) : sqrt(2.) ) * (factor+.01); // note, a 1% safety factor is added here
} 

//! re-sample particles based on an input levelset 
// optionally skip seeding new particles in "exclude" SDF
PYTHON() void adjustNumber(BasicParticleSystem& parts, const MACGrid& vel, const FlagGrid& flags,
                int minParticles, int maxParticles, const LevelsetGrid& phi, Real radiusFactor=1. , Real narrowBand=-1. ,
                const Grid<Real>* exclude=NULL )
{
	// which levelset to use as threshold
	const Real SURFACE_LS = -1.0 * calculateRadiusFactor(phi, radiusFactor);
	Grid<int> tmp( vel.getParent() );
	std::ostringstream out;

	// count particles in cells, and delete excess particles
	for (IndexInt idx=0; idx<(int)parts.size(); idx++) {
		if (parts.isActive(idx)) {
			Vec3i p = toVec3i( parts.getPos(idx) );
			if (!tmp.isInBounds(p) ) {
				parts.kill(idx); // out of domain, remove
				continue;
			}

			Real phiv = phi.getInterpolated( parts.getPos(idx) );
			if( phiv > 0 ) { parts.kill(idx); continue; }
			if( narrowBand>0. && phiv < -narrowBand) { parts.kill(idx); continue; }

			bool atSurface = false;
			if (phiv > SURFACE_LS) atSurface = true;
			int num = tmp(p);
			
			// dont delete particles in non fluid cells here, the particles are "always right"
			if ( num > maxParticles && (!atSurface) ) {
				parts.kill(idx);
			} else {
				tmp(p) = num+1;
			}
		}
	}

	// seed new particles
	RandomStream mRand(9832);
	FOR_IJK(tmp) {
		int cnt = tmp(i,j,k);
		
		// skip cells near surface
		if (phi(i,j,k) > SURFACE_LS) continue;
		if( narrowBand>0. && phi(i,j,k) < -narrowBand ) { continue; }
		if( exclude && ( (*exclude)(i,j,k) < 0.) ) { continue; }

		if (flags.isFluid(i,j,k) && cnt < minParticles) {
			for (int m=cnt; m < minParticles; m++) { 
				Vec3 pos = Vec3(i,j,k) + mRand.getVec3();
				//Vec3 pos (i + 0.5, j + 0.5, k + 0.5); // cell center
				parts.addBuffered( pos ); 
			}
		}
	}

	parts.doCompress();
	parts.insertBufferedParticles();
}

// simple and slow helper conversion to show contents of int grids like a real grid in the ui
// (use eg to quickly display contents of the particle-index grid)
PYTHON() void debugIntToReal(const Grid<int>& source, Grid<Real>& dest, Real factor=1. )
{
	FOR_IJK( source ) { dest(i,j,k) = (Real)source(i,j,k) * factor; }
}

// build a grid that contains indices for a particle system
// the particles in a cell i,j,k are particles[index(i,j,k)] to particles[index(i+1,j,k)-1]
// (ie,  particles[index(i+1,j,k)] already belongs to cell i+1,j,k)
PYTHON() void gridParticleIndex(const BasicParticleSystem& parts, ParticleIndexSystem& indexSys,
                                 const FlagGrid& flags, Grid<int>& index, Grid<int>* counter=NULL )
{
	bool delCounter = false;
	if(!counter) { counter = new Grid<int>(  flags.getParent() ); delCounter=true; }
	else         { counter->clear(); }
	
	// count particles in cells, and delete excess particles
	index.clear();
	int inactive = 0;
	for (IndexInt idx=0; idx<(IndexInt)parts.size(); idx++) {
		if (parts.isActive(idx)) {
			// check index for validity...
			Vec3i p = toVec3i( parts.getPos(idx) );  //p得到粒子所处的cell
			if (! index.isInBounds(p)) { inactive++; continue; }

			index(p)++;               //此时的index的值是包含的粒子数目
		} else {
			inactive++;
		}
	}

	// note - this one might be smaller...
	indexSys.resize( parts.size()-inactive );

	// convert per cell number to continuous index
	IndexInt idx=0;
	FOR_IJK( index ) {
		int num = index(i,j,k);
		index(i,j,k) = idx;               //此时index的值变成包含的第一个粒子的索引
		idx += num; 
	}
	//counter用于记录每个cell所包含的粒子个数
	// add particles to indexed array, we still need a per cell particle counter  
	for (IndexInt idx=0; idx<(IndexInt)parts.size(); idx++) {
		if (!parts.isActive(idx)) continue;
		Vec3i p = toVec3i( parts.getPos(idx) );
		if (! index.isInBounds(p)) { continue; }

		// initialize position and index into original array
		//indexSys[ index(p)+(*counter)(p) ].pos        = parts[idx].pos;
		indexSys[ index(p)+(*counter)(p) ].sourceIndex = idx;
		(*counter)(p)++;
	}
	
	if(delCounter) delete counter;
	//计算粒子间的最小距离，估计直径
// 	Real dis = 1.0;
// 	for (IndexInt idx = 0; idx < (IndexInt)parts.size(); idx++)
// 	{
// 		if (!parts.isActive(idx)) continue;
// 		Vec3i p = toVec3i(parts.getPos(idx));
// 		Vec3 pos = parts.getPos(idx);
// 		IndexInt isysIdxS = index.index(p);              //得到index所在的格子序号
// 		for (int zj = p.x - 1; zj <= p.x + 1; zj++)
// 			for (int yj = p.y - 1; yj <= p.y + 1; yj++)
// 				for (int xj = p.x - 1; xj <= p.x + 1; xj++) {
// 					if (!index.isInBounds(Vec3i(xj, yj, zj))) continue;
// 					IndexInt pStart = index(isysIdxS), pEnd = 0;           //
// 					if (index.isInBounds(isysIdxS + 1)) pEnd = index(isysIdxS + 1);
// 					else                           pEnd = indexSys.size();
// 					for (IndexInt p = pStart; p < pEnd; ++p) {
// 						const int psrc = indexSys[p].sourceIndex;        //得到不按网格排序的粒子索引（即最开始的粒子顺序）
// 						if (idx == psrc)  continue;
// 						const Vec3 ppos = parts[psrc].pos;                //粒子的位置
// 						dis = std::min(dis, fabs(norm(ppos - pos)));
// 					}
// 				}
// 		
// 	}
// 	cout << "粒子间的最小距离是：" << dis << endl;
	//算出的结果大概是10e-2级别  设半径是0.005
}

KERNEL()
void ComputeUnionLevelsetPindex(const Grid<int>& index, const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys,
				LevelsetGrid& phi, const Real radius,
				const ParticleDataImpl<int> *ptype, const int exclude)
{
	const Vec3 gridPos = Vec3(i,j,k) + Vec3(0.5); // shifted by half cell
	Real phiv = radius * 1.0;  // outside           将levelset的初值设为格子对角线的一半
	int r  = int(radius) + 1;     
	int rZ = phi.is3D() ? r : 0;
	for(int zj=k-rZ; zj<=k+rZ; zj++) 
	for(int yj=j-r ; yj<=j+r ; yj++) 
	for(int xj=i-r ; xj<=i+r ; xj++) {
		if (!phi.isInBounds(Vec3i(xj,yj,zj))) continue;

		// note, for the particle indices in indexSys the access is periodic (ie, dont skip for eg inBounds(sx,10,10)
		IndexInt isysIdxS = index.index(xj,yj,zj);           //isysIdxS表示(xj,yj,zj)所在的格子序号       
		IndexInt pStart = index(isysIdxS), pEnd=0;           //
		if(phi.isInBounds(isysIdxS+1)) pEnd = index(isysIdxS+1);
		else                           pEnd = indexSys.size();

		// now loop over particles in cell
		for(IndexInt p=pStart; p<pEnd; ++p) {
			const int psrc = indexSys[p].sourceIndex;        //得到不按网格排序的粒子索引（即最开始的粒子顺序）
			if(ptype && ((*ptype)[psrc] & exclude)) continue;
			const Vec3 pos = parts[psrc].pos;                //粒子的位置
			phiv = std::min( phiv , fabs( norm(gridPos-pos) )-radius );
		}
	}
	//如果网格附近没有粒子，则不执行循环，phiv=radius,
	phi(i,j,k) = phiv;          
}
 
PYTHON() void unionParticleLevelset(const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys,
				    const FlagGrid& flags, const Grid<int>& index, LevelsetGrid& phi, const Real radiusFactor=1.,
				    const ParticleDataImpl<int> *ptype=NULL, const int exclude=0)
{
	// use half a cell diagonal as base radius
	const Real radius = 0.5 * calculateRadiusFactor(phi, radiusFactor);
	// no reset of phi necessary here 
	ComputeUnionLevelsetPindex(index, parts, indexSys, phi, radius, ptype, exclude);

	phi.setBound(0.5, 0);
}
//[2005-Animating Sand as a Fluid]
// Real kernel_function(Real s)
// {
// 	return max(0., pow(1. - pow(s, 2), 3));
// }
// KERNEL()         //针对每一个采样点计算出来一个符号距离场
// void computeSDF(const Grid<int>& index, const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys,
// 	LevelsetGrid& sdf, const Real radius,Vec3 shift,const Grid<Real>& kernelSum,
// 	const ParticleDataImpl<int> *ptype, const int exclude)
// {
// 	//根据采样点调整位置
// 	const Vec3 gridPos = Vec3(i, j, k) + shift;
//     Real phiv = radius * 1.0;  // outside           将levelset的初值设为格子对角线的一半,搜索半径
// 	int r = int(radius) + 1;
// 	
// 	int rZ = sdf.is3D() ? r : 0;
// 	Vec3 averageX=(0.);
// 	
// 	if (kernelSum(i, j, k) < VECTOR_EPSILON) {
// 		sdf(i, j, k) = 0.5;
// 		return;
// 	}
// 	
// 	//计算平均位置
// 	for (int zj = k - rZ; zj <= k + rZ; zj++)
// 		for (int yj = j - r; yj <= j + r; yj++)
// 			for (int xj = i - r; xj <= i + r; xj++) {
// 				if (!sdf.isInBounds(Vec3i(xj, yj, zj))) continue;
// 
// 				// note, for the particle indices in indexSys the access is periodic (ie, dont skip for eg inBounds(sx,10,10)
// 				IndexInt isysIdxS = index.index(xj, yj, zj);  //isysIdxS表示(xj,yj,zj)所在的格子序号（转换成整数）  
// 				IndexInt pStart = index(isysIdxS), pEnd = 0;
// 				if (sdf.isInBounds(isysIdxS + 1)) pEnd = index(isysIdxS + 1);
// 				else                           pEnd = indexSys.size();
// 
// 				// now loop over particles in cell
// 				for (IndexInt p = pStart; p < pEnd; ++p) {
// 					const int psrc = indexSys[p].sourceIndex;        //得到不按网格排序的粒子索引（即最开始的粒子顺序）
// 					if (ptype && ((*ptype)[psrc] & exclude)) continue;
// 					const Vec3 pos = parts[psrc].pos;                //粒子的位置
// 					if (norm(gridPos - pos) > radius)    continue;    
// 					Real dis = norm(gridPos - pos) / radius;
// 					
// 					Real w= kernel_function(dis) / kernelSum(i, j, k);
// 					averageX += w*pos;
// 					//phiv = std::min(phiv, fabs(norm(gridPos - pos)) - radius);
// 				}
// 			}
// 	//计算符号距离场，正号在外边，负号在里边，0是边界 
// 	sdf(i, j, k) = norm(gridPos - averageX) - 0.005;
// 	
// }
// KERNEL()    
// void computeKernelSum(const Grid<int>& index, const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys, 
// 	                  const Real radius,Vec3 shift, Grid<Real>& kernelSum,
// 	                  /* Grid<Real>& kernelSum_x, Grid<Real>& kernelSum_y, Grid<Real>& kernelSum_z,
// 	                   Grid<Real>& kernelSum_12, Grid<Real>& kernelSum_13, Grid<Real>& kernelSum_23*/)
// {
// 	Vec3 samplePoint= Vec3(i, j, k) + shift;
// 	//Vec3 samplePoint = Vec3(i, j, k) + (0.);
// 	//Vec3 samplePoint_12=
// 	int r = int(radius) + 1;
// 	Real w = 0.;              
// 	int rZ = kernelSum.is3D() ? r : 0;
// 	for (int zj = k - rZ; zj <= k + rZ; zj++)
// 		for (int yj = j - r; yj <= j + r; yj++)
// 			for (int xj = i - r; xj <= i + r; xj++) {
// 				IndexInt isysIdxS = index.index(xj, yj, zj);  //isysIdxS表示(xj,yj,zj)所在的格子序号（转换成整数）  
// 				IndexInt pStart = index(isysIdxS), pEnd = 0;
// 				if (kernelSum.isInBounds(isysIdxS + 1)) pEnd = index(isysIdxS + 1);
// 				else                           pEnd = indexSys.size();
// 
// 				for (IndexInt p = pStart; p < pEnd; p++)
// 				{
// 					const int psrc = indexSys[p].sourceIndex;
// 					const Vec3 pos = parts[psrc].pos;
// 					if (norm(samplePoint - pos) > radius)  continue;
// 					w+= max(0., pow(1.-pow(norm(samplePoint-pos)/radius, 2), 3));
// 				}
// 			}
// 	kernelSum(i, j, k) = w;
// 
// }
KERNEL()
void kncomputeLevelSet(const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys,
	                 const FlagGrid& flags, const Grid<int>& index, LevelsetGrid& phi_vertices,
	                 LevelsetGrid& phi_center,LevelsetGrid& phi_facex,LevelsetGrid& phi_facey,
	                 LevelsetGrid& phi_facez,LevelsetGrid& phi_edgex,LevelsetGrid& phi_edgey,
	                 LevelsetGrid& phi_edgez,const Real radius,
	                 const ParticleDataImpl<int> *ptype = NULL, const int exclude = 0)
{
	Vec3 samplePoint_vertices = (i, j, k);
	Vec3 samplePoint_center = (i + 0.5, j + 0.5, k + 0.5);
	Vec3 samplePoint_facex = (i, j + 0.5, k + 0.5);
	Vec3 samplePoint_facey = (i + 0.5, j, k + 0.5);
	Vec3 samplePoint_facez = (i + 0.5, j + 0.5, k);
	Vec3 samplePoint_edgex = (i + 0.5, j, k);
	Vec3 samplePoint_edgey = (i, j + 0.5, k);
	Vec3 samplePoint_edgez = (i, j, k + 0.5);
	Real phiv[8] = { radius * 1.0 };
	int r = (int)radius + 1;
	for (int zj = k - r; zj <= k + r; zj++)
		for (int yj = j - r; yj <= j + r; yj++)
			for (int xj = i - r; xj <= i + r; xj++) {
				if (!phi_vertices.isInBounds(Vec3i(xj, yj, zj))) continue;
				// note, for the particle indices in indexSys the access is periodic (ie, dont skip for eg inBounds(sx,10,10)
				IndexInt isysIdxS = index.index(xj, yj, zj);           //isysIdxS表示(xj,yj,zj)所在的格子序号       
				IndexInt pStart = index(isysIdxS), pEnd = 0;           //
				if (phi_vertices.isInBounds(isysIdxS + 1)) pEnd = index(isysIdxS + 1);
				else                           pEnd = indexSys.size();
				// now loop over particles in cell
				for (IndexInt p = pStart; p < pEnd; ++p) {
					const int psrc = indexSys[p].sourceIndex;        //得到不按网格排序的粒子索引（即最开始的粒子顺序）
					if (ptype && ((*ptype)[psrc] & exclude)) continue;
					const Vec3 pos = parts[psrc].pos;                //粒子的位置

					phiv[0] = std::min(phiv[0], fabs(norm(samplePoint_vertices - pos)) - radius);
					phiv[1] = std::min(phiv[1], fabs(norm(samplePoint_center - pos)) - radius);
					phiv[2] = std::min(phiv[2], fabs(norm(samplePoint_facex - pos)) - radius);
					phiv[3] = std::min(phiv[3], fabs(norm(samplePoint_facey - pos)) - radius);
					phiv[4] = std::min(phiv[4], fabs(norm(samplePoint_facez - pos)) - radius);
					phiv[5] = std::min(phiv[5], fabs(norm(samplePoint_edgex - pos)) - radius);
					phiv[6] = std::min(phiv[6], fabs(norm(samplePoint_edgey - pos)) - radius);
					phiv[7] = std::min(phiv[7], fabs(norm(samplePoint_edgez - pos)) - radius);
				}
			}
	phi_vertices(i, j, k) = phiv[0];
	phi_center(i, j, k) = phiv[1];
	phi_facex(i, j, k) = phiv[2];
	phi_facey(i, j, k) = phiv[3];
	phi_facez(i, j, k) = phiv[4];
	phi_edgex(i, j, k) = phiv[5];
	phi_edgey(i, j, k) = phiv[6];
	phi_edgez(i, j, k) = phiv[7];
}
//计算四面体体积
Real computeTetrahedronVolume(Vec3 pos1, Vec3 pos2, Vec3 pos3, Vec3 pos4) {
	Vec3 a = pos2 - pos1;
	Vec3 b = pos3 - pos1;
	Vec3 c = pos4 - pos1;
	Vec3 d = (a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
	return norm(dot(d, c)) / 6.0;
}


Real computeTetrahedron(Vec3 *pos, Real phi[], int negative_count)
{
	Vec3 negative_pos[4] = { (0.) }, zero_pos[4] = { (0.) };
	int mark[4] = { 5 };
	Real tmp1=0., tmp2=0., tmp3=0.;
 int i,j;
	switch (negative_count)
	{
	case 0:
		return 0.;
		break;//四面体全部为流体
	case 1:
		//Vec3 negative_pos, zero_pos[3]; int mark;
		for (i = 0; i < 4; i++)
		{
			if (phi[i] < 0.)
			{
				negative_pos[0] = pos[i];                                  //记录phi为负的位置
				mark[0] = i;                                               //标记出负的索引
				break;
			}
		}
		for (i = 0, j = 0; i < 4; i++)
		{
			if (i == mark[0])   continue;
			zero_pos[j++] = pos[i] * (phi[mark[0]] / (phi[mark[0]] - phi[i])) - pos[mark[0]] * (phi[i] / (phi[mark[0]] - phi[i]));
		}
		return computeTetrahedronVolume(negative_pos[0], zero_pos[0], zero_pos[1], zero_pos[2]);   break;       //计算出流体体积
	case 2:

		for (i = 0, j = 0; i < 4; i++)
		{
			if (phi[i] < 0.)
			{
				negative_pos[j] = pos[i];
				mark[j++] = i;
			}
		}
		for (int i = 0, j = 0; i < 4; i++)
		{
			if (i == mark[0] || i == mark[1])          continue;
			zero_pos[j++] = pos[i] * (phi[mark[0]] / (phi[mark[0]] - phi[i])) - pos[mark[0]] * (phi[i] / (phi[mark[0]] - phi[i]));
			zero_pos[j++] = pos[i] * (phi[mark[1]] / (phi[mark[1]] - phi[i])) - pos[mark[1]] * (phi[i] / (phi[mark[1]] - phi[i]));
		}
		//得到两个负的phi值点和四个零值点
		 tmp1 = computeTetrahedronVolume(negative_pos[0], negative_pos[1], zero_pos[0], zero_pos[3]);
		 tmp2 = computeTetrahedronVolume(negative_pos[1], zero_pos[1], zero_pos[0], zero_pos[3]);
		 tmp3 = computeTetrahedronVolume(negative_pos[0], zero_pos[2], zero_pos[0], zero_pos[3]);
		return tmp1 + tmp2 + tmp3;    break;
	case 3:
		for (int i = 0, j = 0; i < 4; i++)
		{
			if (phi[i] < 0.)
			{
				negative_pos[j] = pos[i];
				mark[j++] = i;
			}
		}
		for (int i = 0, j = 0; i < 4; i++)
		{
			if (i == mark[0] || i == mark[1] || i == mark[2])      continue;
			zero_pos[j++] = pos[i] * (phi[mark[0]] / (phi[mark[0]] - phi[i])) - pos[mark[0]] * (phi[i] / (phi[mark[0]] - phi[i]));
			zero_pos[j++] = pos[i] * (phi[mark[1]] / (phi[mark[1]] - phi[i])) - pos[mark[1]] * (phi[i] / (phi[mark[1]] - phi[i]));
			zero_pos[j++] = pos[i] * (phi[mark[2]] / (phi[mark[2]] - phi[i])) - pos[mark[2]] * (phi[i] / (phi[mark[2]] - phi[i]));
		}
		//得到三个负的phi值点和三个零值点
		 tmp1 = computeTetrahedronVolume(negative_pos[0], negative_pos[1], negative_pos[2], zero_pos[1]);
		 tmp2 = computeTetrahedronVolume(negative_pos[0], negative_pos[2], zero_pos[0], zero_pos[1]);
		 tmp3 = computeTetrahedronVolume(negative_pos[2], zero_pos[0], zero_pos[1], zero_pos[2]);
		return tmp1 + tmp2 + tmp3;   break;
	case 4:
		return computeTetrahedronVolume(pos[0], pos[1], pos[2], pos[3]);   break;
	default:
		return 0.;
	}
}     
//[Geometric integration over irregular domains with application to level - set methods]
KERNEL(bnd=1)              //输入是四个点的phi值和位置，计算四面体中的流体体积
void computeVolume(LevelsetGrid& phi_center, LevelsetGrid& phi_vertices, Grid<Real>& volume_center,Vec3 shift, Vec3i firstIndex)
{
	//暂时未考虑边界情况
	
	//把正方体分解成四个四面体,
	Vec3 gridPos = (i, j, k) + shift;
	Vec3 pos[8] = { gridPos + (-0.5,-0.5,-0.5),gridPos + (0.5,-0.5,-0.5),gridPos + (0.5,0.5,-0.5),gridPos + (-0.5,0.5,-0.5),
				 gridPos + (-0.5,-0.5,0.5),gridPos + (0.5,-0.5,0.5),gridPos + (0.5,0.5,0.5),gridPos + (-0.5,0.5,0.5) };
	Real phiv[8];
	Vec3i startIndex = (i, j, k) + firstIndex;
	//得到八个点的phi值
		phiv[0] = phi_vertices(startIndex);
	    phiv[1] = phi_vertices(startIndex+(1 , 0, 0));
		phiv[2] = phi_vertices(startIndex + ( 1,  1, 0));
		phiv[3] = phi_vertices(startIndex + (0,  1, 0));

		phiv[4] = phi_vertices(startIndex + (0, 0,  1));
		phiv[5] = phi_vertices(startIndex + ( 1, 0,  1));
		phiv[6] = phi_vertices(startIndex + ( 1,  1,  1));
		phiv[7] = phi_vertices(startIndex + (0, 1,  1));

	
	//分解成五个四面体  phiv[0 1 3 4],  phiv[1 2 3 6]  , phiv[3 4 6 7],    phiv[1 4 5 6]  phiv[1 3 4 6]
	int negative_count[5] = { 0 };
	for (int i = 0; i < 8; i++)
	{
		if (phiv[i] < 0.)
		{
			switch (i) {
			case 0:
				negative_count[0]++;  break;
			case 1:
				negative_count[0]++;   negative_count[1]++; negative_count[3]++;   negative_count[4]++;   break;
			case 2:
				negative_count[1]++;   break;
			case 3:
				negative_count[0]++;   negative_count[1]++;    negative_count[2]++;   negative_count[4]++;   break;
			case 4:
				negative_count[0]++;   negative_count[2]++;   negative_count[3]++;     negative_count[4]++;   break;
			case 5:
				negative_count[3]++;   break;
			case 6:
				negative_count[1]++;   negative_count[2]++;   negative_count[3]++;   negative_count[4]++;   break;
			case 7:
				negative_count[2]++;   break;
			}
		}
	}
	//整理四个顶点位置，四个顶点的phi值
	Vec3 pos1[4] = { pos[0],pos[1],pos[3],pos[4] };
	Real phi1[4] = { phiv[0],phiv[1],phiv[3],phiv[4] };
	Vec3 pos2[4] = { pos[1],pos[2],pos[3],pos[6] };
	Real phi2[4] = { phiv[1],phiv[2],phiv[3],phiv[6] };
	Vec3 pos3[4] = { pos[3],pos[4],pos[6],pos[7] };
	Real phi3[4] = { phiv[3],phiv[4],phiv[6],phiv[7] };
	Vec3 pos4[4] = { pos[1],pos[4],pos[5],pos[6] };
	Real phi4[4] = { phiv[1],phiv[4],phiv[5],phiv[6] };
	Vec3 pos5[4] = { pos[1],pos[3],pos[4],pos[6] };
	Real phi5[4] = { phiv[1],phiv[3],phiv[4],phiv[6] };
	Real tmp1=computeTetrahedron(pos1, phi1, negative_count[0]);
	Real tmp2=computeTetrahedron(pos2, phi2, negative_count[1]);
	Real tmp3 = computeTetrahedron(pos3, phi3, negative_count[2]);
	Real tmp4 = computeTetrahedron(pos4, phi4, negative_count[3]);
	Real tmp5 = computeTetrahedron(pos5, phi5, negative_count[4]);
	volume_center(i, j, k) = tmp1 + tmp2 + tmp3 + tmp4 + tmp5;
}


//[2008---Accurate Viscous Free Surfaces for Buckling, Coiling, and Rotating Liquids]
PYTHON() void computeVolumeWeight(const  BasicParticleSystem& parts, const ParticleIndexSystem& indexSys,
	const FlagGrid& flags, const Grid<int>& index, LevelsetGrid& phi_center,
	LevelsetGrid& phi_vertices,
	LevelsetGrid& phi_facex, LevelsetGrid &phi_facey,LevelsetGrid& phi_facez,
	LevelsetGrid& phi_edgex,LevelsetGrid& phi_edgey,LevelsetGrid& phi_edgez,const Real radiusFactor = 1.,
	const ParticleDataImpl<int> *ptype = NULL, const int exclude = 0)
{
	
	FluidSolver* parent = flags.getParent();
	//取cell对角线的一半作为搜索半径
	const Real radius = 0.5 * calculateRadiusFactor(phi_center, radiusFactor);
	//根据phi值，计算出体积
	Grid<Real> volume_center(parent), volume_facex(parent), volume_facey(parent), 
		volume_facez(parent), volume_edgex(parent), volume_edgey(parent), volume_edgez(parent);
	//计算体积权重的采样点包括 格子中心，三个面的中心，三条边的中心
	//[2003-flip]    先计算每个位置的权重和,存在一个real网格中
	/*Grid<Real> kernel_center(parent), kernel_12(parent), kernel_13(parent), kernel_23(parent), kernel_x(parent), kernel_y(parent), kernel_z(parent);
	computeKernelSum(index,parts,indexSys,   radius,Vec3(0.5,0.5,0.5),kernel_center);
	//以边的中点作为采样点
	computeKernelSum(index, parts, indexSys, radius, Vec3(0., 0., 0.5), kernel_12);   
	computeKernelSum(index, parts, indexSys, radius, Vec3(0., 0.5, 0.), kernel_13);
	computeKernelSum(index, parts, indexSys, radius, Vec3(0.5, 0., 0.), kernel_23);
	//以面的中心作为采样点
	computeKernelSum(index, parts, indexSys, radius, Vec3(0., 0.5, 0.5), kernel_x);
	computeKernelSum(index, parts, indexSys, radius, Vec3(0.5, 0., 0.5), kernel_y);
	computeKernelSum(index, parts, indexSys, radius, Vec3(0.5, 0.5, 0.), kernel_z);
	*/
	//得到权重和后，根据此计算符号距离场
	//ComputeUnionLevelsetPindex(index, parts, indexSys, phi, radius, ptype, exclude);
	//computeLevelelset();
	
	kncomputeLevelSet(parts, indexSys, flags, index, phi_vertices, phi_center, phi_facex, phi_facey, phi_facez, phi_edgex,
		phi_edgey, phi_edgez, radius, ptype, exclude);
	phi_vertices.setBound(0.5, 0);
	phi_center.setBound(0.5, 0);
	phi_facex.setBound(0.5, 0);
	phi_facey.setBound(0.5, 0);
	phi_facez.setBound(0.5, 0);
	phi_edgex.setBound(0.5, 0);
	phi_edgey.setBound(0.5, 0);
	phi_edgez.setBound(0.5, 0);
 	computeVolume(phi_center, phi_vertices, volume_center,(0.5,0.5,0.5), (0, 0, 0));
 	computeVolume(phi_edgex, phi_facex, volume_edgex, (0.5,0.,0.),(0, -1, -1));
 	computeVolume(phi_edgey, phi_facey, volume_edgey, (0.,0.5,0.),(-1, 0, -1));
 	computeVolume(phi_edgez, phi_facez, volume_edgez, (0.,0.,0.5),(-1, -1, 0));
 	computeVolume(phi_facex, phi_edgex, volume_facex, (0.,0.5,0.5),(-1, 0, 0));
 	computeVolume(phi_facey, phi_edgey, volume_facey, (0.5,0.,0.5),(0, -1, 0));
 	computeVolume(phi_facez, phi_edgez, volume_facez,(0.5,0.5,0.),(0, 0, -1));
	volume_edgex.setBoundNeumann(1);
	volume_edgey.setBoundNeumann(1);
	volume_edgez.setBoundNeumann(1);
	volume_facex.setBoundNeumann(1);
	volume_facey.setBoundNeumann(1);
	volume_facez.setBoundNeumann(1);
	//computeVolume();
}

//! kernel for computing averaged particle level set weights
KERNEL()
void ComputeAveragedLevelsetWeight(const BasicParticleSystem& parts,
				   const Grid<int>& index, const ParticleIndexSystem& indexSys,
				   LevelsetGrid& phi, const Real radius,
				   const ParticleDataImpl<int>* ptype, const int exclude,
				   Grid<Vec3>* save_pAcc = NULL, Grid<Real>* save_rAcc = NULL)
{
	const Vec3 gridPos = Vec3(i,j,k) + Vec3(0.5); // shifted by half cell
	Real phiv = radius * 1.0; // outside 

	// loop over neighborhood, similar to ComputeUnionLevelsetPindex
	const Real sradiusInv = 1. / (4. * radius * radius) ;
	int   r = int(1. * radius) + 1;
	int   rZ = phi.is3D() ? r : 0;
	// accumulators
	Real  wacc = 0.;
	Vec3  pacc = Vec3(0.);
	Real  racc = 0.;

	for(int zj=k-rZ; zj<=k+rZ; zj++) 
	for(int yj=j-r ; yj<=j+r ; yj++) 
	for(int xj=i-r ; xj<=i+r ; xj++) {
		if (! phi.isInBounds(Vec3i(xj,yj,zj)) ) continue;

		IndexInt isysIdxS = index.index(xj,yj,zj);
		IndexInt pStart = index(isysIdxS), pEnd=0;
		if(phi.isInBounds(isysIdxS+1)) pEnd = index(isysIdxS+1);
		else                           pEnd = indexSys.size();
		for(IndexInt p=pStart; p<pEnd; ++p) {
			IndexInt   psrc = indexSys[p].sourceIndex;
			if(ptype && ((*ptype)[psrc] & exclude)) continue;

			Vec3  pos  = parts[psrc].pos; 
			Real  s    = normSquare(gridPos-pos) * sradiusInv;
			//Real  w = std::max(0., cubed(1.-s) );
			Real  w = std::max(0., (1.-s)); // a bit smoother
			wacc += w;
			racc += radius * w;
			pacc += pos    * w;
		} 
	}

	if(wacc > VECTOR_EPSILON) {
		racc /= wacc;
		pacc /= wacc;
		phiv = fabs( norm(gridPos-pacc) )-racc;

		if (save_pAcc) (*save_pAcc)(i, j, k) = pacc;
		if (save_rAcc) (*save_rAcc)(i, j, k) = racc;
	}
	phi(i,j,k) = phiv;
}

template<class T> T smoothingValue(const Grid<T> val, int i, int j, int k, T center) {
	return val(i,j,k);
}

// smoothing, and  
KERNEL(bnd=1) template<class T> 
void knSmoothGrid(const Grid<T>& me, Grid<T>& tmp, Real factor) {
	T val = me(i,j,k) + 
			me(i+1,j,k) + me(i-1,j,k) + 
			me(i,j+1,k) + me(i,j-1,k) ;
	if(me.is3D()) {
		val += me(i,j,k+1) + me(i,j,k-1);
	}
	tmp(i,j,k) = val * factor;
}

KERNEL(bnd=1) template<class T> 
void knSmoothGridNeg(const Grid<T>& me, Grid<T>& tmp, Real factor) {
	T val = me(i,j,k) + 
			me(i+1,j,k) + me(i-1,j,k) + 
			me(i,j+1,k) + me(i,j-1,k) ;
	if(me.is3D()) {
		val += me(i,j,k+1) + me(i,j,k-1);
	}
	val *= factor;
	if(val<tmp(i,j,k)) tmp(i,j,k) = val;
	else               tmp(i,j,k) = me(i,j,k);
}

//! Zhu & Bridson particle level set creation 
PYTHON() void averagedParticleLevelset(const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys,
				       const FlagGrid& flags, const Grid<int>& index, LevelsetGrid& phi, const Real radiusFactor=1.,
				       const int smoothen=1, const int smoothenNeg=1,
				       const ParticleDataImpl<int>* ptype=NULL, const int exclude=0)
{
	// use half a cell diagonal as base radius
	const Real radius = 0.5 * calculateRadiusFactor(phi, radiusFactor); 
	ComputeAveragedLevelsetWeight(parts, index, indexSys, phi, radius, ptype, exclude);

	// post-process level-set
	for(int i=0; i<std::max(smoothen,smoothenNeg); ++i) {
		LevelsetGrid tmp(flags.getParent());
		if(i<smoothen) { 
			knSmoothGrid    <Real> (phi,tmp, 1./(phi.is3D() ? 7. : 5.) );
			phi.swap(tmp);
		}
		if(i<smoothenNeg) { 
			knSmoothGridNeg <Real> (phi,tmp, 1./(phi.is3D() ? 7. : 5.) );
			phi.swap(tmp);
		}
	} 
	phi.setBound(0.5, 0);
}

//! kernel for improvedParticleLevelset
KERNEL(bnd=1)
void correctLevelset(LevelsetGrid& phi, const Grid<Vec3>& pAcc, const Grid<Real>& rAcc,
					const Real radius, const Real t_low, const Real t_high)
{
	if (rAcc(i, j, k) <= VECTOR_EPSILON) return; //outside nothing happens
	Real x = pAcc(i, j, k).x;
	
	// create jacobian of pAcc via central differences
	Matrix3x3f jacobian = Matrix3x3f(
		0.5 * (pAcc(i+1, j,   k  ).x - pAcc(i-1, j,   k  ).x),
		0.5 * (pAcc(i,   j+1, k  ).x - pAcc(i,   j-1, k  ).x),
		0.5 * (pAcc(i,   j  , k+1).x - pAcc(i,   j,   k-1).x),
		0.5 * (pAcc(i+1, j,   k  ).y - pAcc(i-1, j,   k  ).y),
		0.5 * (pAcc(i,   j+1, k  ).y - pAcc(i,   j-1, k  ).y),
		0.5 * (pAcc(i,   j,   k+1).y - pAcc(i,   j,   k-1).y),
		0.5 * (pAcc(i+1, j,   k  ).z - pAcc(i-1, j,   k  ).z),
		0.5 * (pAcc(i,   j+1, k  ).z - pAcc(i,   j-1, k  ).z),
		0.5 * (pAcc(i,   j,   k+1).z - pAcc(i,   j,   k-1).z)
	);

	// compute largest eigenvalue of jacobian
	Vec3 EV = jacobian.eigenvalues();
	Real maxEV = std::max(std::max(EV.x, EV.y), EV.z);

	// calculate correction factor
	Real correction = 1;
	if (maxEV >= t_low) {
		Real t = (t_high - maxEV) / (t_high - t_low);
		correction = t*t*t - 3 * t*t + 3 * t;
	}
	correction = (correction < 0) ? 0 : correction; // enforce correction factor to [0,1] (not explicitly in paper)

	const Vec3 gridPos = Vec3(i, j, k) + Vec3(0.5); // shifted by half cell
	const Real correctedPhi = fabs(norm(gridPos - pAcc(i, j, k))) - rAcc(i, j, k) * correction;
	phi(i, j, k) = (correctedPhi > radius) ? radius : correctedPhi; // adjust too high outside values when too few particles are
																	// nearby to make smoothing possible (not in paper)
}

//! Approach from "A unified particle model for fluid-solid interactions" by Solenthaler et al. in 2007
PYTHON() void improvedParticleLevelset(const BasicParticleSystem& parts, const ParticleIndexSystem& indexSys, const FlagGrid& flags,
	const Grid<int>& index, LevelsetGrid& phi, const Real radiusFactor = 1., const int smoothen = 1,const int smoothenNeg = 1,
	const Real t_low = 0.4, const Real t_high = 3.5, const ParticleDataImpl<int>* ptype = NULL, const int exclude = 0)
{
	// create temporary grids to store values from levelset weight computation
	Grid<Vec3> save_pAcc(flags.getParent());
	Grid<Real> save_rAcc(flags.getParent());

	const Real radius = 0.5 * calculateRadiusFactor(phi, radiusFactor); // use half a cell diagonal as base radius
	ComputeAveragedLevelsetWeight(parts, index, indexSys, phi, radius, ptype, exclude, &save_pAcc, &save_rAcc);
	correctLevelset(phi, save_pAcc, save_rAcc, radius, t_low, t_high);

	// post-process level-set
	for (int i = 0; i<std::max(smoothen, smoothenNeg); ++i) {
		LevelsetGrid tmp(flags.getParent());
		if (i<smoothen) {
			knSmoothGrid    <Real>(phi, tmp, 1. / (phi.is3D() ? 7. : 5.));
			phi.swap(tmp);
		}
		if (i<smoothenNeg) {
			knSmoothGridNeg <Real>(phi, tmp, 1. / (phi.is3D() ? 7. : 5.));
			phi.swap(tmp);
		}
	}
	phi.setBound(0.5, 0);
}


KERNEL(pts)
void knPushOutofObs(BasicParticleSystem& parts, const FlagGrid& flags, const Grid<Real>& phiObs, const Real shift, const Real thresh,
		    const ParticleDataImpl<int>* ptype, const int exclude) {
	if (!parts.isActive(idx) || (ptype && ((*ptype)[idx] & exclude))) return;
	Vec3i p = toVec3i( parts.getPos(idx) );

	if (!flags.isInBounds(p)) return;
	Real v = phiObs.getInterpolated(parts.getPos(idx));
	if(v < thresh) {
		Vec3 grad = getGradient( phiObs, p.x,p.y,p.z );
		if( normalize(grad) < VECTOR_EPSILON ) return;
		parts.setPos(idx, parts.getPos(idx) + grad*(thresh - v + shift));
	}
}
//! push particles out of obstacle levelset
PYTHON() void pushOutofObs(BasicParticleSystem& parts, const FlagGrid& flags, const Grid<Real>& phiObs, const Real shift=0, const Real thresh=0,
			   const ParticleDataImpl<int>* ptype=NULL, const int exclude=0) {
	knPushOutofObs(parts, flags, phiObs, shift, thresh, ptype, exclude);
}

//******************************************************************************
// grid interpolation functions

KERNEL(idx) template<class T> 
void knSafeDivReal(Grid<T>& me, const Grid<Real>& other, Real cutoff=VECTOR_EPSILON) { 
	if(other[idx]<cutoff) {
		me[idx] = 0.;
	} else {
		T div( other[idx] );
		me[idx] = safeDivide(me[idx], div ); 
	}
}

// Set velocities on the grid from the particle system

KERNEL(pts, single)
void knMapLinearVec3ToMACGrid(const BasicParticleSystem& p, const FlagGrid& flags, const MACGrid& vel, Grid<Vec3>& tmp,
			      const ParticleDataImpl<Vec3>& pvel, const ParticleDataImpl<int>* ptype, const int exclude)
{
	unusedParameter(flags);
	if (!p.isActive(idx) || (ptype && ((*ptype)[idx] & exclude))) return;
	vel.setInterpolated( p[idx].pos, pvel[idx], &tmp[0] );
}

// optionally , this function can use an existing vec3 grid to store the weights
// this is useful in combination with the simple extrapolation function
PYTHON() void mapPartsToMAC(const FlagGrid& flags, MACGrid& vel, MACGrid& velOld,
			    const BasicParticleSystem& parts, const ParticleDataImpl<Vec3>& partVel, Grid<Vec3>* weight=NULL,
			    const ParticleDataImpl<int>* ptype=NULL, const int exclude=0)
{
	// interpol -> grid. tmpgrid for particle contribution weights
	bool freeTmp = false;
	if(!weight) {
		weight = new Grid<Vec3>(flags.getParent());
		freeTmp = true;
	} else {
		weight->clear(); // make sure we start with a zero grid!
	}
	vel.clear();
	knMapLinearVec3ToMACGrid( parts, flags, vel, *weight, partVel, ptype, exclude );

	// stomp small values in weight to zero to prevent roundoff errors
	weight->stomp(Vec3(VECTOR_EPSILON));
	vel.safeDivide(*weight);
	
	// store original state
	velOld.copyFrom( vel );
	if(freeTmp) delete weight;
}

KERNEL(pts, single) template<class T>
void knMapLinear(const BasicParticleSystem& p, const FlagGrid& flags, const Grid<T>& target, Grid<Real>& gtmp,
        const ParticleDataImpl<T>& psource )
{
	unusedParameter(flags);
	if (!p.isActive(idx)) return;
	target.setInterpolated( p[idx].pos, psource[idx], gtmp );
} 

template<class T>
void mapLinearRealHelper(const FlagGrid& flags, Grid<T>& target,
                const BasicParticleSystem& parts, const ParticleDataImpl<T>& source )
{
	Grid<Real> tmp(flags.getParent());
	target.clear();
	knMapLinear<T>( parts, flags, target, tmp, source ); 
	knSafeDivReal<T>( target, tmp );
}

PYTHON() void mapPartsToGrid    (const FlagGrid& flags, Grid<Real>& target , const BasicParticleSystem& parts , const ParticleDataImpl<Real>& source ) {
	mapLinearRealHelper<Real>(flags,target,parts,source);
}
PYTHON() void mapPartsToGridVec3(const FlagGrid& flags, Grid<Vec3>& target , const BasicParticleSystem& parts , const ParticleDataImpl<Vec3>& source ) {
	mapLinearRealHelper<Vec3>(flags,target,parts,source);
}
// integers need "max" mode, not yet implemented
//PYTHON() void mapPartsToGridInt ( FlagGrid& flags, Grid<int >& target , BasicParticleSystem& parts , ParticleDataImpl<int >& source ) {
//	mapLinearRealHelper<int >(flags,target,parts,source);
//}

KERNEL(pts) template<class T>
void knMapFromGrid(const BasicParticleSystem& p, const Grid<T>& gsrc, ParticleDataImpl<T>& target )
{
	if (!p.isActive(idx)) return;
	target[idx] = gsrc.getInterpolated( p[idx].pos );
} 
PYTHON() void mapGridToParts    (const Grid<Real>& source , const BasicParticleSystem& parts , ParticleDataImpl<Real>& target ) {
	knMapFromGrid<Real>(parts, source, target);
}
PYTHON() void mapGridToPartsVec3(const Grid<Vec3>& source , const BasicParticleSystem& parts , ParticleDataImpl<Vec3>& target ) {
	knMapFromGrid<Vec3>(parts, source, target);
}


// Get velocities from grid

KERNEL(pts) 
void knMapLinearMACGridToVec3_PIC(const BasicParticleSystem& p, const FlagGrid& flags, const MACGrid& vel, ParticleDataImpl<Vec3>& pvel,
				  const ParticleDataImpl<int>* ptype, const int exclude)
{
	if (!p.isActive(idx) || (ptype && ((*ptype)[idx] & exclude))) return;
	// pure PIC
	pvel[idx] = vel.getInterpolated( p[idx].pos );
}
PYTHON() void mapMACToParts(const FlagGrid& flags, const MACGrid& vel ,
                            const BasicParticleSystem& parts , ParticleDataImpl<Vec3>& partVel,
			    const ParticleDataImpl<int>* ptype=NULL, const int exclude=0) {
	knMapLinearMACGridToVec3_PIC( parts, flags, vel, partVel, ptype, exclude );
}

// with flip delta interpolation 
KERNEL(pts) 
void knMapLinearMACGridToVec3_FLIP(const BasicParticleSystem& p, const FlagGrid& flags, const MACGrid& vel, const MACGrid& oldVel,
				   ParticleDataImpl<Vec3>& pvel,
				   const Real flipRatio, const ParticleDataImpl<int>* ptype, const int exclude)
{
	if (!p.isActive(idx) || (ptype && ((*ptype)[idx] & exclude))) return;
	Vec3 v     =        vel.getInterpolated(p[idx].pos);
	Vec3 delta = v - oldVel.getInterpolated(p[idx].pos); 
	pvel[idx] = flipRatio * (pvel[idx] + delta) + (1.0 - flipRatio) * v;    
}

PYTHON() void flipVelocityUpdate(const FlagGrid& flags, const MACGrid& vel, const MACGrid& velOld,
				 const BasicParticleSystem& parts, ParticleDataImpl<Vec3>& partVel, const Real flipRatio,
				 const ParticleDataImpl<int>* ptype=NULL, const int exclude=0) {
	knMapLinearMACGridToVec3_FLIP( parts, flags, vel, velOld, partVel, flipRatio, ptype, exclude );
}


//******************************************************************************
// narrow band 

KERNEL()
void knCombineVels(MACGrid& vel, const Grid<Vec3>& w, MACGrid& combineVel, const LevelsetGrid* phi, Real narrowBand, Real thresh ) {
	int idx = vel.index(i,j,k);

	for(int c=0; c<3; ++c)
	{
			// Correct narrow-band FLIP
			if(phi) {
				Vec3 pos(i,j,k);
				pos[(c+1)%3] += Real(0.5);
				pos[(c+2)%3] += Real(0.5);
				Real p = phi->getInterpolated(pos);
				if (p < -narrowBand) { vel[idx][c] = 0; continue; }
			} 

			if (w[idx][c] > thresh) {
				combineVel[idx][c] = vel[idx][c];
				vel[idx][c] = -1;
			} else {
				vel[idx][c] = 0;
			}
	}
}

//! narrow band velocity combination
PYTHON() void combineGridVel( MACGrid& vel, const Grid<Vec3>& weight, MACGrid& combineVel, const LevelsetGrid* phi=NULL,
    Real narrowBand=0.0, Real thresh=0.0) {
	knCombineVels(vel, weight, combineVel, phi, narrowBand, thresh);
}

//! surface tension helper
PYTHON() void getLaplacian(Grid<Real> &laplacian, const Grid<Real> &grid) {
	LaplaceOp(laplacian, grid);
}


} // namespace

